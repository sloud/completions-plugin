export ZSH_COMPLETIONS="${ZSH_COMPLETIONS:-$HOME/.local/completions}"
fpath+=("$ZSH_COMPLETIONS")

if [ ! -d "$ZSH_COMPLETIONS" ]; then
    mkdir -p "$ZSH_COMPLETIONS" 2>&1 >/dev/null
fi
